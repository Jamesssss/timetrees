using System;
using System.IO;
using System.Globalization;
using Microsoft.VisualBasic;
using Newtonsoft.Json;
using System.Text;
using System.Collections.Generic;
namespace TimeTrees
{
    struct Person
    {
        public int id;
        public string name;
        public DateTime? birthday;
        public DateTime? deathday;
        public List<int?> parents;

        public void DisplayInfo()
        {
            Console.WriteLine($"id: {id}  name: {name}  birthday: {birthday}  deathday: {deathday}");
        }
    }
    struct TimelineEvent
    {
        public string description;
        public DateTime? date;
        public List<int> identifiers;
        public void DisplayInfo()
        {
            Console.WriteLine($"Description: {description}  Date: {date}");
        }
    }
    public static class DateTimeException
    {
        public static string CorrectDelta(this int Day)
        {
            int years = Day / 365;
            int months = (Day % 365) / 30;
            int days = Day % 30;
            return $"Между событиями прошло {years} лет, {months} месяцев и {days} дней";
        }
    }
    class Program
    {
        private const int idIndex = 0;
        private const int nameIndex = 3;
        private const int parentIndex1 = 1; 
        private const int parentIndex2 = 2;
        private const int birthdayIndex = 4;
        private const int deathdayIndex = 5;

        private const int DeltaProgramId        = 1;     //delta
        private const int LeapYearProgramId     = 2;     //leap
        private const int AddEventProgramId     = 3;     //addEvent
        private const int AddPersonProgramId    = 4;     //addPerson
        private const int FindPersonProgramid   = 5;     //findPerson
        private const int ExitId                = 6;



        public const string timelineFileName    = "timeline.csv";
        public const string peopleFileName      = "people.csv";
        public static TimelineEvent[] ChooseFileExtensionForTimeline(string timelineFileName)
        {
            TimelineEvent[] timeline;
            

            if (timelineFileName == "timeline.csv")
                timeline = ReadTimeline("..\\..\\..\\..\\..\\" + timelineFileName);
            else
                timeline = ReadTimelineFromJson("..\\..\\..\\..\\..\\" + timelineFileName).ToArray();
            return timeline;
        }
        public static  Person[] ChooseFileExtensionForPeople(string peopleFileName)
        {
            Person[] people;
            if (peopleFileName == "people.csv")
                people = ReadPeople("..\\..\\..\\..\\..\\" + peopleFileName);
            else
                people = ReadPeopleFromJson("..\\..\\..\\..\\..\\" + peopleFileName);
            return people;
        }
        static Person[] ReadPeople(string path)
        {
            string[] data = File.ReadAllLines(path);
            Person[] splitData = new Person[data.Length];
            for (var i = 0; i < data.Length; i++)
            {
                Person human;      //Id;
                var line = data[i];
                string[] parts = line.Split(";");
                human.id = int.Parse(parts[idIndex]);
                human.name = parts[nameIndex];
                human.birthday = DateParse(parts[birthdayIndex]);
                if (parts.Length > 5)
                    human.deathday = DateParse(parts[deathdayIndex]);
                else
                    human.deathday = null;  
                human.parents = new List<int?> { int.Parse(parts[parentIndex1]), int.Parse(parts[parentIndex2]) };
                splitData[i] = human;
            }
            return splitData;
        }
        static TimelineEvent[] ReadTimeline(string path)
        {
            string[] data = File.ReadAllLines(path);
            TimelineEvent[] splitData = new TimelineEvent[data.Length];
            for (var i = 0; i < data.Length; i++)
            {
                TimelineEvent significantEvent;
                var line = data[i];
                string[] parts = line.Split(";"); 
                significantEvent.date = DateParse(parts[0]);
                significantEvent.description = parts[1];
                significantEvent.identifiers = new List<int> { };
                splitData[i] = significantEvent;
            }
            return splitData;
        }
        static (DateTime, DateTime) GetDate(TimelineEvent[] timeline)
        {
            DateTime maxDate = DateTime.MinValue;
            DateTime minDate = DateTime.MaxValue;
            for (var j = 0; j < timeline.Length; j++)
            {
                var date = timeline[j].date;
                if (date > maxDate) maxDate =(DateTime) date;
                if (date < minDate) minDate = (DateTime) date;
            }
            return (maxDate, minDate);
            
        }
        static bool DetectLeapYear(int year)
        {
            if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0)
            {
                return true;
            }
            else return false;
        }
        static Person[] ReadPeopleFromJson(string path)
        {   
            string json = File.ReadAllText(path);
            var jsonList = JsonConvert.DeserializeObject<List<Person>>(json);
            var people = jsonList.ToArray();
            return people;
        }
        static List<TimelineEvent> ReadTimelineFromJson(string path)
        {
            string json = File.ReadAllText(path);
            return JsonConvert.DeserializeObject<List<TimelineEvent>>(json);
        }
        static void ExecuteDelta()
        {
            
            var timeline = ChooseFileExtensionForTimeline(timelineFileName);
            (var maxDate, var minDate) = GetDate(timeline);
            var deltaDate = maxDate - minDate;
            Console.WriteLine(deltaDate.Days.CorrectDelta());
            Console.WriteLine("Нажмите ESC, чтобы выйти в главное меню...");
        }
        static void ExecuteLeapYear()
        {
            
            var people = ChooseFileExtensionForPeople(peopleFileName);
            foreach (var person in people)
            {
                var data = (DateTime)person.birthday;
                var year = data.Year;
                if (DetectLeapYear(year) && (DateTime.Now.Year - year <= 20))
                    Console.WriteLine(person.name);
                //  person.DisplayInfo();
            }
            Console.WriteLine("Нажмите ESC, чтобы выйти в главное меню...");
        }
        static void Main(string[] args)
        {
            Menu();
            
           /* var people = ChooseFileExtensionForPeople(peopleFileName);
            var timeline = ChooseFileExtensionForTimeline(timelineFileName);
            WriteJson(people, timeline);*/

        }
        static void WriteJson(Person[] people, TimelineEvent[] timeline)
        {
            string outTimeline = JsonConvert.SerializeObject(timeline);
            string outPeople = JsonConvert.SerializeObject(people);
            File.WriteAllText(@"C:\dev\timetrees\timeline.json", outTimeline, Encoding.UTF8);
            File.WriteAllText(@"C:\dev\timetrees\people.json", outPeople, Encoding.UTF8);
        }
        public static DateTime DateParse(string value)
        {
            DateTime date;
            if (!DateTime.TryParseExact(value, "yyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
            {
                if (!DateTime.TryParseExact(value, "yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    if (!DateTime.TryParseExact(value, "yyyy-MM", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                    {
                        if (!DateTime.TryParseExact(value, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                        {
                            throw new Exception("Wrong format");
                        }
                    }
                }
            }
            return date;

        }
        static bool PersonIsInFile(Person[] people, int id)
        {


            foreach (var person in people)
            {
                if (person.id == id)
                {
                    return true;
                }
                
            }
            return false;

        }
        public class MenuItem
        {
            public int Id;
            public string Text;
            public bool IsSelected;

        }
        static void ExecuteAddPerson()
        {
            var people = ChooseFileExtensionForPeople(peopleFileName);
            Person newPerson;
            Console.WriteLine("Введите имя человека: ");
            newPerson.name = Console.ReadLine();
            Console.Clear();
            Console.WriteLine("Введите дату рождения человека: ");
            newPerson.birthday = null;
            var birthdayDate = Console.ReadLine();   
            while (true)
            {
                try
                {
                    newPerson.birthday = DateParse(birthdayDate);
                }
                catch (Exception)
                {
                    Console.Clear();
                    Console.WriteLine("Неверный формат даты... ");
                    Console.WriteLine("Нажмите ESC, чтобы выйти в главное меню...");
                    Console.WriteLine("Попробуйте ввести еще раз: ");
                    var keyInfo = Console.ReadKey();
                    if (keyInfo.Key == ConsoleKey.Escape)
                    {
                        ConsoleCleaner();
                        Menu();
                    }
                    else
                    {
                        birthdayDate = keyInfo.KeyChar +  Console.ReadLine();
                    } 
                    
                }
                if (newPerson.birthday != null)
                    break;
            }
            Console.Clear();
            Console.WriteLine("Введите дату смерти человека: ");
            Console.WriteLine("Если человек еще жив, ничего не вводите и нажмите Enter...");
            var deathDate = Console.ReadLine();
            if (deathDate == "")
                newPerson.deathday = null;
            else
            {
                newPerson.deathday = null;
                while (true)
                {
                    try
                    {
                        newPerson.deathday = DateParse(deathDate);
                    }
                    catch(Exception)
                    {
                        ConsoleCleaner();
                        Console.WriteLine("Неверный формат даты... ");
                        Console.WriteLine("Нажмите ESC, чтобы выйти в главное меню...");
                        Console.WriteLine("Попробуйте ввести еще раз: ");
                        var keyInfo = Console.ReadKey();
                        if (keyInfo.Key == ConsoleKey.Escape)
                        {
                            ConsoleCleaner();
                            Menu();
                        }
                        else
                        {
                            deathDate = keyInfo.KeyChar + Console.ReadLine();
                            continue;
                        }
                    }
                    if (newPerson.deathday != null)
                    {
                        if (newPerson.birthday > newPerson.deathday)
                        {
                            ConsoleCleaner();
                            Console.WriteLine("Дата смерти не может быть меньше даты рождения! Попробуйте еще раз...");
                            deathDate = Console.ReadLine();
                            continue;
                        }
                        else break;
                    }
                    else break;
                }
            }
            ConsoleCleaner();
            List<int?> IDs = new() { };
            var id = SubMenuForAddNewPerson();
            if (id != 0)
            {
                IDs.Add(id);
                id = SubMenuForAddNewPerson();
                IDs.Add(id);
            }
            else
            {
                IDs.Add(id);
                IDs.Add(id);
                
            }
           
            ConsoleCleaner();
            newPerson.id = people.Length + 1;
            newPerson.parents = IDs;
            var addablePerson = NewPersonInformationToString(newPerson);
            WriteNewPersonStringToFile(addablePerson);
            ConsoleCleaner();
            Console.WriteLine("Человек успешно сохранен! ");
            Console.WriteLine("Нажмите ESC, чтобы выйти в главное меню...");

        }
        static string NewPersonInformationToString(Person newPerson)
        {
            string stringToReturn;
            if (newPerson.deathday != null)
            {
                return stringToReturn = $"{Environment.NewLine}{newPerson.id};{String.Join(";", newPerson.parents.ToArray())};{newPerson.name};{DatetimeToString(newPerson.birthday)};{DatetimeToString(newPerson.deathday)}";
            }
            else
            {
                return stringToReturn = $"{Environment.NewLine}{newPerson.id};{String.Join(";", newPerson.parents.ToArray())};{newPerson.name};{DatetimeToString(newPerson.birthday)}{DatetimeToString(newPerson.deathday)}";
            }
            
        }
        static void WriteNewPersonStringToFile(string personInformation)
        {
            using (StreamWriter ple = new StreamWriter("..\\..\\..\\..\\..\\people.csv", true, System.Text.Encoding.UTF8))
            {

                ple.Write(personInformation);
            }
        }
        static void ExecuteAddParticipantForNewEvent()
        {
            var people = ChooseFileExtensionForPeople(peopleFileName);
            List<int> IDs = new();
            Console.WriteLine("Введите Id участника: ");
            for (int i = 0; ;i++)
            {
                if (i == 1)
                    Console.WriteLine("Поробуйте еще раз или нажмите ESC, чтобы перейти к сохранию...");
                string strId = Console.ReadLine();
                ConsoleCleaner();
                var id = int.Parse(strId);
                if (PersonIsInFile(people, id))
                {
                    IDs.Add(id);
                    break;
                }
                else
                    Console.WriteLine("Такого человека нет в списке.");
            }
        }
        static void ExecuteSearchPerson()
        {
            List<MenuItem> publicGeneratedMenu = new();
            var people = ChooseFileExtensionForPeople(peopleFileName);
            List<Person> publicSuitablePeople = new();
            List<string> symbles = new();
            Console.WriteLine("Начните вводить имя человека: ");
            var keyInfo = Console.ReadKey();
            while (true)
            {
                List<MenuItem> generatedMenu = new();
                List<Person> suitablePeople = new();
                if (keyInfo.Key == ConsoleKey.Backspace)
                {
                    symbles.RemoveAt(symbles.Count-1);
                    ConsoleCleaner();
                    Console.WriteLine($"Найдено {publicGeneratedMenu.Count} человек(а): ");
                    MenuDrawer(generatedMenu);
                    keyInfo = Console.ReadKey();
                    continue;
                }
                ConsoleCleaner();
                var symble = keyInfo.KeyChar.ToString();
                symbles.Add(symble);
                
                var partOFName = String.Join("", symbles);
                for (int p = 0;p<people.Length;p++)
                {
                    if (people[p].name.IndexOf(partOFName) == 0)
                    {
                        suitablePeople.Add(people[p]);
                        if (generatedMenu.Count == 0)
                            generatedMenu.Add(new MenuItem { Text = /*$"{p + 1}." +*/ people[p].name, Id = DeltaProgramId, IsSelected = true });
                        else
                            generatedMenu.Add(new MenuItem { Text = /*$"{p + 1}." +*/ people[p].name, Id = DeltaProgramId });

                    }

                }
                publicGeneratedMenu = generatedMenu;
                publicSuitablePeople = suitablePeople;
                ConsoleCleaner();
                Console.WriteLine($"По запросу \"{partOFName}\" найдено {publicGeneratedMenu.Count} человек(а): ");
                MenuDrawer(generatedMenu);
                keyInfo = Console.ReadKey();
                if (keyInfo.Key == ConsoleKey.UpArrow || keyInfo.Key == ConsoleKey.DownArrow || keyInfo.Key == ConsoleKey.Enter)
                    break;
            }
            while (true)
            {             
                if (keyInfo.Key == ConsoleKey.DownArrow)
                    NextSelectedPoint(publicGeneratedMenu);
                if (keyInfo.Key == ConsoleKey.UpArrow)
                    PrevSelectedPoint(publicGeneratedMenu);
                ConsoleCleaner();
                Console.WriteLine($"Найдено {publicGeneratedMenu.Count} человек(а): ");
                MenuDrawer(publicGeneratedMenu);
                if (keyInfo.Key == ConsoleKey.Enter)
                {
                    ConsoleCleaner();
                    for (int i = 0; i <publicGeneratedMenu.Count;i++)
                    {
                        if (publicGeneratedMenu[i].IsSelected)
                        {
                            Console.WriteLine($"ID: {publicSuitablePeople[i].id}");
                            Console.WriteLine($"Имя: {publicSuitablePeople[i].name}");
                            Console.WriteLine($"Дата рождения: {publicSuitablePeople[i].birthday}");
                            Console.WriteLine($"Дата смерти: {publicSuitablePeople[i].deathday}");
                            if (publicSuitablePeople[i].parents[0] == 0)
                            {
                                Console.WriteLine("Информация о родителях не найдена.");
                            }                           
                            if (publicSuitablePeople[i].parents[1] != 0)
                            {
                                Console.Write($"Родители: {people[(int)publicSuitablePeople[i].parents[0] - 1].name}, ");
                                Console.WriteLine(people[(int)publicSuitablePeople[i].parents[1] - 1].name);
                            }
                            if (publicSuitablePeople[i].parents[0] != 0 && publicSuitablePeople[i].parents[1] == 0)
                                Console.WriteLine($"Родители: {people[(int)publicSuitablePeople[i].parents[0] - 1].name} ");
                            Console.WriteLine("Нажмите ESC, чтобы выйти в главное меню...");
                        }                         
                    }
                }
                if (keyInfo.Key == ConsoleKey.Escape)
                {
                    ConsoleCleaner();
                    Menu();
                }
                keyInfo = Console.ReadKey();
            }
        }
        static void ExecuteAddEvent()
        {
            
            var people = ChooseFileExtensionForPeople(peopleFileName);
            TimelineEvent newEvent;
            Console.WriteLine("Нажмите ESC, чтобы выйти в главное меню...");
            Console.WriteLine("Введите дату события: ");
            if (Console.ReadKey().Key == ConsoleKey.Escape)
            {
                ConsoleCleaner();
                Menu();
            }
            var date = Console.ReadLine();
            newEvent.date = null;
            while (true)
            {
                try
                {
                    newEvent.date = DateParse(date);
                }
                catch (Exception)
                {

                    ConsoleCleaner();
                    Console.WriteLine("Неверный формат даты... ");
                    Console.WriteLine("Попробуйте ввести еще раз: ");
                    date = Console.ReadLine();
                }
                if (newEvent.date != null)
                    break;
            }
            ConsoleCleaner();
            Console.WriteLine("Нажмите ESC, чтобы выйти в главное меню...");
            Console.WriteLine("Введите описание события: ");
            if (Console.ReadKey().Key == ConsoleKey.Escape)
            {
                ConsoleCleaner();
                Menu();
            }
                
            newEvent.description = Console.ReadLine();
            ConsoleCleaner();
            //Menu3();
            Console.WriteLine("Введите Id участника: ");
            List<int> IDs = new();
            while (true)
            {
                
                string strId = Console.ReadLine();
                ConsoleCleaner();
                if (strId == "")
                {
                    Console.WriteLine("Событие успешно сохранено! ");
                    Console.WriteLine("Нажмите ESC, чтобы выйти в главное меню...");
                    break;
                }
                var id = int.Parse(strId);
               
                if (PersonIsInFile(people, id))
                {
                    IDs.Add(id);
                }
                else
                    Console.WriteLine("Такого человека нет в списке.");
                Console.WriteLine("Если вы указали всех участников, нажмите ENTER, чтобы сохранить...");
                Console.WriteLine("ИЛИ");
                Console.WriteLine("Продолжайте вводить участников: ");                
            }
            newEvent.identifiers = IDs;
            var AddableEvent = NewEventInformationToString(newEvent);
            WriteNewEventStringToFile(AddableEvent);


        }
        public static int? SubMenuForAddNewPerson()
        {
            //List<MenuItem> PrevGeneratedMenu = new();
            var publicPartOfName = "";
            List<MenuItem> publicGeneratedMenu = new();
            var people = ChooseFileExtensionForPeople(peopleFileName);
            List<Person> publicSuitablePeople = new();
            List<string> symbles = new();
            Console.WriteLine("Начните вводить имя родителя: ");
            Console.WriteLine("Если вы укзали всех родителей или их нет, нажмите S, чтобы сохранить...");
            var keyInfo = Console.ReadKey();
            while (true)
            {
                List<MenuItem> generatedMenu = new();
                List<Person> suitablePeople = new();
                if (keyInfo.Key == ConsoleKey.S)
                    return 0;
                if (keyInfo.Key == ConsoleKey.Backspace)
                {

                    symbles.RemoveAt(symbles.Count - 1);
                    ConsoleCleaner();
                    
                    //Console.WriteLine($"Найдено {publicGeneratedMenu.Count} человек(а): ");
                    Console.WriteLine($"По запросу \"{publicPartOfName}\" найдено {publicGeneratedMenu.Count} человек(а): ");
                    MenuDrawer(generatedMenu);
                    keyInfo = Console.ReadKey();
                    continue;
                }
                //ConsoleCleaner();
                var symble = keyInfo.KeyChar.ToString();
                symbles.Add(symble);

                var partOFName = String.Join("", symbles);
                publicPartOfName = partOFName;
                for (int p = 0; p < people.Length; p++)
                {
                    if (people[p].name.IndexOf(partOFName) == 0)
                    {
                        suitablePeople.Add(people[p]);
                        if (generatedMenu.Count == 0)
                            generatedMenu.Add(new MenuItem { Text = /*$"{p + 1}." +*/ people[p].name, Id = DeltaProgramId, IsSelected = true });
                        else
                            generatedMenu.Add(new MenuItem { Text = /*$"{p + 1}." +*/ people[p].name, Id = DeltaProgramId });

                    }

                }
                publicGeneratedMenu = generatedMenu;
                publicSuitablePeople = suitablePeople;
                ConsoleCleaner();
                Console.WriteLine($"По запросу \"{partOFName}\" найдено {publicGeneratedMenu.Count} человек(а): ");
                Console.WriteLine("Выберете родителя из списка и нажмите ENTER...");
                MenuDrawer(generatedMenu);
                keyInfo = Console.ReadKey();
                if (keyInfo.Key == ConsoleKey.UpArrow || keyInfo.Key == ConsoleKey.DownArrow || keyInfo.Key == ConsoleKey.Enter)
                    break;
            }
            while (true)
            {
                if (keyInfo.Key == ConsoleKey.DownArrow)
                    NextSelectedPoint(publicGeneratedMenu);
                if (keyInfo.Key == ConsoleKey.UpArrow)
                    PrevSelectedPoint(publicGeneratedMenu);
                ConsoleCleaner();
                Console.WriteLine($"По запросу \"{publicPartOfName}\" найдено {publicGeneratedMenu.Count} человек(а): ");
                Console.WriteLine("Выберете родителя из списка и нажмите ENTER...");
                MenuDrawer(publicGeneratedMenu);
                if (keyInfo.Key == ConsoleKey.Enter)
                {
                    ConsoleCleaner();
                    for (int i = 0; i < publicGeneratedMenu.Count;i++)
                    {
                        if (publicGeneratedMenu[i].IsSelected)
                        {
                            
                            return publicSuitablePeople[i].id;
                        }
                    }
                }
                

                if (keyInfo.Key == ConsoleKey.Escape)
                {
                    ConsoleCleaner();
                    Menu();
                }
                keyInfo = Console.ReadKey();
            }
        }
        static void ExecuteSaveAndExit()
        {
            
        }

        public static void Menu3()
        {
            List<MenuItem> menu = new()
            {
                new MenuItem { Text = "1.Добавить участника", Id = 0, IsSelected = true },
                new MenuItem { Text = "2.Сохранить и выйти", Id = 1 }
            };
            MenuDrawer(menu);
            while (true)
            {
                var keyInfo = Console.ReadKey();
                if (keyInfo.Key == ConsoleKey.DownArrow)
                    NextSelectedPoint(menu);
                if (keyInfo.Key == ConsoleKey.UpArrow)
                    PrevSelectedPoint(menu);
                ConsoleCleaner();
                MenuDrawer(menu);
                if (keyInfo.Key == ConsoleKey.Enter)
                {
                    ConsoleCleaner();
                    foreach (var item in menu)
                    {
                        if (item.IsSelected)
                            ExecuteForMenu3(item.Id);
                    }
                }
                if (Console.ReadKey().Key == ConsoleKey.Escape)
                {
                    ConsoleCleaner();
                    Menu();
                }
            }
        }
        static string DatetimeToString(DateTime? date)
        {
            var str = date.ToString();
            if (str != "")
                return DateTime.Parse(str).ToString("yyyy-MM-dd");
            else
                return null;
        }
        static string NewEventInformationToString(TimelineEvent newEvent)
        {
            string stringToReturn = $"{Environment.NewLine}{DatetimeToString(newEvent.date)};{newEvent.description};{String.Join(";", newEvent.identifiers.ToArray())}";
            return stringToReturn;
            
        }
        static void WriteNewEventStringToFile(string personInformation)
        {
            using (StreamWriter ple = new StreamWriter("..\\..\\..\\..\\..\\timeline.csv", true, System.Text.Encoding.UTF8))
            {
                
                ple.Write(personInformation);
            }
        }
        static void Execute(int value)
        {
            switch (value)
            {
                case DeltaProgramId:
                    ExecuteDelta();
                    break;
                case LeapYearProgramId:
                    ExecuteLeapYear();
                    break;
                case AddEventProgramId:
                    ExecuteAddEvent();
                    break;
                case AddPersonProgramId:
                    ExecuteAddPerson();
                    break;
                case FindPersonProgramid:
                    ExecuteSearchPerson();
                    break;
                case ExitId:
                    Environment.Exit(0);
                    break;
                default:
                    Console.WriteLine("Данная функция пока недоступна.");
                    break;
            }
        }
        static void ExecuteForMenu3(int value)
        {
            switch (value)
            {
                case 1:
                    ExecuteAddParticipantForNewEvent();
                    break;
                case 2:
                    ExecuteSaveAndExit();
                    break;
                default:
                    Console.WriteLine("Данная функция пока недоступна.");
                    break;
            }
        }
        public static void Menu()
        {
            Console.CursorVisible = false;
            List<MenuItem> menu = new ()
            {
                new MenuItem {Text = "1.Найти, сколько лет прошло между ранним и поздним событием.", Id = DeltaProgramId, IsSelected = true},
                new MenuItem {Text = "2.Найти человека моложе 20 лет и родившегося в високосынй год", Id = LeapYearProgramId },
                new MenuItem {Text = "3.Добавить событие в БД", Id = AddEventProgramId },
                new MenuItem {Text = "4.Добавить человека в БД", Id = AddPersonProgramId},
                new MenuItem {Text = "5.Посмотреть информацию  о человеке", Id = FindPersonProgramid },
                new MenuItem {Text = "6.Выход", Id = ExitId }

            };
            MenuDrawer(menu);
            while (true)
            {
                var keyInfo = Console.ReadKey();
                if (keyInfo.Key == ConsoleKey.DownArrow)
                    NextSelectedPoint(menu);
                if (keyInfo.Key == ConsoleKey.UpArrow)
                    PrevSelectedPoint(menu);
                ConsoleCleaner();
                MenuDrawer(menu);
                if (keyInfo.Key == ConsoleKey.Enter)
                {
                    ConsoleCleaner();
                    foreach (var item in menu)
                    {
                        if (item.IsSelected)
                            Execute(item.Id);

                    }
                }
                if (keyInfo.Key == ConsoleKey.Escape)
                {
                    ConsoleCleaner();
                    MenuDrawer(menu);
                }

            }
        }
        static void NextSelectedPoint(List<MenuItem> menu)
        {
            for (int i = 0; i < menu.Count; i++)
            {
                if (menu[i].IsSelected)
                {
                    menu[i].IsSelected = false;
                    i = i == menu.Count - 1
                         ? 0
                         : ++i;
                    menu[i].IsSelected = true;
                }
            }
        }
        static void PrevSelectedPoint(List<MenuItem> menu)
        {
            for (int i = 0; i < menu.ToArray().Length; i++)
            {
                if (menu[i].IsSelected)
                {
                    menu[i].IsSelected = false;
                    i = i == 0
                         ? menu.Count
                         : i--;
                    menu[i - 1].IsSelected = true;
                }
            }
        }
        public static void MenuDrawer(List<MenuItem> menu)
        {
            //Console.WriteLine("Выберите действие: ");
            foreach (MenuItem item in menu)
            {
                if (item.IsSelected)
                    Console.BackgroundColor = ConsoleColor.Red;
                else
                    Console.BackgroundColor = ConsoleColor.Black;
                Console.WriteLine(item.Text);
            }
            Console.BackgroundColor = ConsoleColor.Black;
        }
        static void ConsoleCleaner()
        {
            for (int i = 0; i < Console.WindowHeight; i++)
            {
                Console.SetCursorPosition(0, i);
                Console.Write(new String(' ', Console.WindowWidth));
            }
            Console.SetCursorPosition(0, 0);
        }
    }


}
